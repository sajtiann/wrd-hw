package hu.wrd.shapes;

public interface Shape extends Comparable<Shape> {

    public double getArea();

    @Override
    default int compareTo(Shape o) {
        double areaDiff = getArea() - o.getArea();
        if (areaDiff > 0) {
            return 1;
        } else if (areaDiff < 0) {
            return -1;
        } else {
            return 0;
        }
    }
}
