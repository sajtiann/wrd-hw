package hu.wrd.shapes;

public class Square implements Shape {

    private double x;

    Square(double x) throws IllegalArgumentException {
        if (x <= 0) {
            throw new IllegalArgumentException("The side of the square (x) must be greater than zero!");
        }
        this.x = x;
    }

    @Override
    public double getArea() {
        return x * x;
    }

    @Override
    public String toString() {
        return "square's sides: " + x
                + ", square's area : " + getArea();
    }

}
