package hu.wrd.shapes;

public class Circle implements Shape {

    private double radius;

    Circle(double radius) throws IllegalArgumentException {
        if (radius <= 0) {
            throw new IllegalArgumentException("The radius must be greater than 0!");
        }
        this.radius = radius;
    }

    @Override
    public double getArea() {
        return radius * radius * Math.PI;
    }

    @Override
    public String toString() {
        return "circle's radius: " + radius
                + ", circle's area : " + getArea();
    }

}
