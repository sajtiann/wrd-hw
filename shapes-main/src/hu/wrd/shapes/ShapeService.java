package hu.wrd.shapes;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class ShapeService {

    private final ArrayList<Shape> shapes = new ArrayList<>();

    private boolean check(Shape one, Shape two) {
        if (Math.abs(one.getArea() - two.getArea()) == 0) {
            if (one.getClass() == two.getClass()) {
                return true;
            } else if (one.getClass() == Square.class
                    && two.getClass() == Rectangle.class) {
                Rectangle rect = (Rectangle) two;
                if (rect.isSquare()) {
                    return true;
                }
            } else if (two.getClass() == Square.class
                    && one.getClass() == Rectangle.class) {
                Rectangle rect = (Rectangle) one;
                if (rect.isSquare()) {
                    return true;
                }
            }
        }
        return false;
    }

    public void addShapes(Shape... shapes) {
        //TODO implement
        for (int i = 1; i < shapes.length; i++) {
            Shape shape1 = shapes[i - 1];
            Shape shape2 = shapes[i];
            if (check(shape1, shape2)) {
                System.err.println("New shapes must be unique!");
                return;
            }
        }
        for (Shape inputShape : shapes) {
            for (Shape arrayShape : this.shapes) {
                if (check(inputShape, arrayShape)) {
                    System.err.println("New shapes must be unique!");
                    return;
                }
            }
        }
        this.shapes.addAll(Arrays.asList(shapes));
    }

    private void printShapes() {
        for (Shape shape : this.shapes) {
            System.out.println(shape);
        }
    }

    public void printShapesOrderByAreaAsc() {
        //TODO implement
        System.out.println("printShapesOrderByAreaAsc() called");
        Collections.sort(this.shapes);
        printShapes();
    }

    public void printShapesOrderByAreaDesc() {
        //TODO implement
        System.out.println("printShapesOrderByAreaDesc() called");
        Collections.sort(this.shapes, Collections.reverseOrder());
        printShapes();
    }
}
