package hu.wrd.shapes;

public class Rectangle implements Shape {

    private double x;
    private double y;

    Rectangle(double x, double y) throws IllegalArgumentException {
        if (x > 0 && y > 0) {
            this.x = x;
            this.y = y;
        } else {
            throw new IllegalArgumentException("The sides of the rectangle (x, y) must be greater than 0!");
        }
    }

    public boolean isSquare() {
        return x == y;
    }

    @Override
    public double getArea() {
        return x * y;
    }

    @Override
    public String toString() {
        return "rectangle's sides: " + x + ", " + y
                + "; rectangle's area: " + getArea();
    }

}
