package hu.wrd.shapes;

public class Main {

    public static void main(String[] args) {

        ShapeService shapeService = new ShapeService();

        shapeService.addShapes(
                new Square(1),
                new Square(2),
                //new Square(2),
                new Square(3),
                new Rectangle(2, 3),
                new Rectangle(2, 4),
                new Rectangle(3, 4),
                new Rectangle(3, 5),
                new Rectangle(4, 5),
                new Circle(1),
                new Circle(2),
                new Circle(3));

        // Rectangle(3, 3) is the same shape as new Square(3) added previously, so none of these 3 shapes should be added.
        // Please handle this error without exiting the program
        shapeService.addShapes(
                new Square(4),
                //new Rectangle(3, 3),
                //new Rectangle(7, 3),
                //new Rectangle(3, 7),
                new Circle(4));

        shapeService.printShapesOrderByAreaAsc();

        System.out.println("-----------------");

        shapeService.printShapesOrderByAreaDesc();

    }
}
