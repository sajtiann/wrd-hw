# Shapes

This is a command-line Java application, which can be started using the `main()` method in the `Main` class.

Import project into your favourite Java IDE and mark folder `src` as a source folder.

Specification:

1. Please write Java code without adding any 3rd party library.

2. Your goal is to implement functionality in ShapeService. Feel free to add or change code in any other places too!

3. ShapeService should contain business logic which handles 2-dimensional Shapes. It is a stateful service, Shape instances can be added to it using void addShapes(Shape ... shapes) method

4. Implement Rectangle, Circle and Square shapes.

5. Inside ShapeService, please implement printShapesOrderByAreaAsc() and printShapesOrderByAreaDesc() methods which print the previously added shapes in order based on their area in ascending or in descending order respectively.
Make sure that output is human-readable;

6. Ensure that only real 2D shape instances can be created (ie. no negative or zero size is accepted).

7. ShapeService.addShapes() must ensure that the same shape is added only once.
If a duplicated shape is passed in addShapes(), no shapes should be added at all (not even the new shapes which are not duplicates).
(2 shapes are considered to be duplicated if they look exactly the same. For example a new Rectangle(2, 5) is considered to be the same shape as new Rectangle(5, 2).
Furthermore, new Square(5) is considered to be the same shape as new Rectangle(5, 5).)